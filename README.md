# Home.js
Here's where I'm developing node.js modules for controlling various devices you would find around your home using node.js as a server for servicing these devices. I eventually hope to extend functionality to be voice controlled via Amazon Echo as soon as resources are made available to me. On top of it all there's a few client side APIs to be thrown in as well.

---------------------------------------------------------------------------------------------------------------------

This is the list of features I hope to add eventually. Most all of these features revolve around using Amazon Echo to invoke a command; however, they will be able to be used without an echo.

Support for the following:

1. Chromecast API
2. Youtube API
3. Amazon Echo API
4. Spotify API
5. Univeral remote via Arduino and IR sender
6. Home automation hardware (lights, thermostats, locks, and doors)
